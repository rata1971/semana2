package com.example.semana2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ConfirmActivity extends AppCompatActivity {
    private String name;
    private String date;
    private String email;
    private String phone;
    private String desc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        Intent intent = getIntent();
        Bundle extra = intent.getExtras();
        name = extra.get(getResources().getString(R.string.pNombre)).toString();
        date = extra.get(getResources().getString(R.string.pFecha)).toString();
        email = extra.get(getResources().getString(R.string.pEmail)).toString();
        phone = extra.get(getResources().getString(R.string.pTelefono)).toString();
        desc = extra.get(getResources().getString(R.string.pDescripcion)).toString();

        ((TextView)findViewById(R.id.tvName)).setText(name);
        ((TextView)findViewById(R.id.tvDate)).setText(date);
        ((TextView)findViewById(R.id.tvEmail)).setText(email);
        ((TextView)findViewById(R.id.tvPhone)).setText(phone);
        ((TextView)findViewById(R.id.tvDesc)).setText(desc);

        Button edit = (Button) findViewById(R.id.btnEdit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                intent.putExtra(getResources().getString(R.string.pNombre).toString(), name);
                intent.putExtra(getResources().getString(R.string.pFecha), date);
                intent.putExtra(getResources().getString(R.string.pEmail), email);
                intent.putExtra(getResources().getString(R.string.pTelefono), phone);
                intent.putExtra(getResources().getString(R.string.pDescripcion), desc);

                startActivity(intent);
            }
        });
    }
}