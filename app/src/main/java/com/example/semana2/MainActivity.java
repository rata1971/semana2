package com.example.semana2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView name;
    private DatePicker date;
    private TextView phone;
    private TextView email;
    private TextView desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button siguiente = (Button) findViewById(R.id.btnSiguiente);
        TextView name = (TextView) findViewById(R.id.etName);
        DatePicker date = (DatePicker) findViewById(R.id.etDate);
        TextView phone = (TextView) findViewById(R.id.etPhone);
        TextView email = (TextView) findViewById(R.id.etEmail);
        TextView desc = (TextView) findViewById(R.id.etDesc);


        Intent intent = getIntent();
        Bundle extra = intent.getExtras();
        if ( extra != null ) {
            name.setText( extra.get(getResources().getString(R.string.pNombre)).toString());

            String sDate = extra.get(getResources().getString(R.string.pFecha)).toString();
            String[] aDate = sDate.split("/");

            date.updateDate(Integer.parseInt(aDate[2]),
                    Integer.parseInt(aDate[1])-1,
                    Integer.parseInt(aDate[0])
                    );

            email.setText(extra.get(getResources().getString(R.string.pEmail)).toString());
            phone.setText(extra.get(getResources().getString(R.string.pTelefono)).toString());
            desc.setText(extra.get(getResources().getString(R.string.pDescripcion)).toString());
        }

        siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ConfirmActivity.class);
                TextView name = (TextView) findViewById(R.id.etName);
                DatePicker date = (DatePicker) findViewById(R.id.etDate);
                TextView phone = (TextView) findViewById(R.id.etPhone);
                TextView email = (TextView) findViewById(R.id.etEmail);
                TextView desc = (TextView) findViewById(R.id.etDesc);

                intent.putExtra(getResources().getString(R.string.pNombre).toString(), name.getText());
                intent.putExtra("nombre", name.getText());
                intent.putExtra(getResources().getString(R.string.pFecha), date.getDayOfMonth() + "/" + (date.getMonth()+1) + "/" + date.getYear());
                intent.putExtra(getResources().getString(R.string.pEmail), email.getText());
                intent.putExtra(getResources().getString(R.string.pTelefono), phone.getText());
                intent.putExtra(getResources().getString(R.string.pDescripcion), desc.getText());

                startActivity(intent);
            }
        });
    }
}